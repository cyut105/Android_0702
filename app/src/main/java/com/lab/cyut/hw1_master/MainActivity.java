package com.lab.cyut.hw1_master;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/*
 * Company: CYUT
 * User: Zion
 * Date: 2018/07/04
 * Version: 1.0
 * Since: SDK 25
 */
public class MainActivity extends AppCompatActivity {

    //region 變數
    private TextView tv1;
    private Button btn1,btn2,btn3,btn4;
    int sum=0;
    //endregion

    //region onCreate
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findView();
        setListener();
    }
    //endregion

    //region 連結畫面
    private void findView(){
        tv1=(TextView)findViewById(R.id.tv1);
        btn1=(Button)findViewById(R.id.btn1);
        btn2=(Button)findViewById(R.id.btn2);
        btn3=(Button)findViewById(R.id.btn3);
        btn4=(Button)findViewById(R.id.btn4);
    }
    //endregion

    //region 設置監聽器
    private void setListener(){
        btn1.setOnClickListener(onClickListener);
        btn2.setOnClickListener(onClickListener);
        btn3.setOnClickListener(onClickListener);
        btn4.setOnClickListener(onClickListener);
    }
    //endregion

    //region 按鈕監聽器
    private View.OnClickListener onClickListener =new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btn1:
                    sum+=1;
                    tv1.setText(String.valueOf(sum));
                    break;
                case R.id.btn2:
                    sum*=2;
                    tv1.setText(String.valueOf(sum));
                    break;
                case R.id.btn3:
                    sum/=3;
                    tv1.setText(String.valueOf(sum));
                    break;
                case R.id.btn4:
                    Toast.makeText(MainActivity.this,String.valueOf(sum),Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
    //endregion
    
}
